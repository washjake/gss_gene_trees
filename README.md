# README #

This pipeline uses scripts written by Jacob Washburn as well as scripts written by Ya Yang (https://bitbucket.org/yangya/phylogenomic_dataset_construction).  Some of the Ya Yang scripts are used as received from her and others are modified slightly.

The first few steps have a simple or single-file command example as well as a multi-file command for processing files in parallel. These are to help the user understand the process and allow for practice/ troubleshooting.  After the first few steps all commands are given in the multi-file format as this is the way most users will need them.

#Steps for using pipeline#

##1 Check sequence quality (I use fastqc; http://www.bioinformatics.babraham.ac.uk/projects/fastqc/)

single-file command:
	
	sbatch --wrap"fastqc --outdir fastqc_reports/ --noextract fastq_file_R1.fastq

multi-file command:

	for file in *.fastq; do sbatch --wrap="fastqc --outdir fastqc_reports/ --noextract ${file}"; done

##2 Trim/filter sequences (I use prinseq; http://prinseq.sourceforge.net/), concatenate single and paired read files for simplicity.

single-file command:

	sbatch --wrap="perl prinseq-lite.pl -fastq fastq_file_R1.fastq -fastq2 fastq_file_R2.fastq -min_len 30 -min_qual_score 24 -trim_left 15"

multi-file command:

	for file in *R1_001.fastq; do sbatch --wrap="perl prinseq-lite.pl -fastq ${file} -fastq2 ${file%R1_001.fastq}R2_001.fastq -min_len 30 -min_qual_score 24 -trim_left 15"; done

single-file commands:

	sbatch --wrap="cat fastq_file_R1_good_XXXX.fastq fastq_file_R1_good_singletons.fastq > fastq_file_R1_good.fastq"
	sbatch --wrap="cat fastq_file_R2_good_XXXX.fastq fastq_file_R2_good_singletons.fastq > fastq_file_R2_good.fastq"

multi-file commands:

	for file in *R1_001_prinseq_good_singletons*; do sbatch --wrap="cat ${file%singletons*}* > ${file%_singletons*}.fastq"; done
	for file in *R2_001_prinseq_good_singletons*; do sbatch --wrap="cat ${file%singletons*}* > ${file%_singletons*}.fastq"; done

##3 Prepare fasta files for blast database.  Use coding sequences (Proteins) from closely related species.  Can use 1 to many species but they must have the same gene annotation names (i.e., gene=rps12 in fasta header).  My scripts are designed to work with files downloaded from the NCBI Organelle Genome Resources database; http://www.ncbi.nlm.nih.gov/genome/organelle/.

#a) concatenate all fasta files for database into one:

example command:

	cat A_thaliana_chloroplast_pep.fasta B_napus_chloroplast_pep.fasta > database_file.fasta
 
#b) preform the following commands on the concatenated file from above to remove delta sequences and format for use in my scripts 

	grep '^-' database_file.fasta 
	sed -i 's/^-//' database_file.fasta
	sed -i -e 's/*//g' database_file.fasta

##4 Blast reads in each file from step 2 against database using 4_blastx_sorter.py script.

single-file command:

	module load ncbi/blast-2.2.30+
	sbatch --wrap="python 4_blastx_sorter.py fastq_file_R1_good_singletons.fastq database_file.fasta

multi-file command:

	module load ncbi/blast-2.2.30+
	for file in *good.fastq; do sbatch --wrap="python 4_blastx_sorter.py ${file} database_file.fasta"; done

##5 Run prinseq again to fix unmatched pairs

example command:

	for file in *R1_001_prinseq_good.fastq_contigs_that_match_database_file.fasta.fastq; do sbatch --wrap="perl prinseq-lite.pl -fastq ${file} -fastq2 ${file%R1_001_prinseq_good.fastq_contigs_that_match_database_file.fasta.fastq}R2_001_prinseq_good.fastq_contigs_that_match_database_file.fasta.fastq -min_len 30 -min_qual_score 24"; done

##6 Concatenate singletons and remove original singleton files

example commands:

	for file in *R1_001_prinseq_good_prinseq_good_singletons*; do cat ${file} ${file%R1_001_prinseq_good_prinseq_good_singletons*}R2_001_prinseq_good_prinseq_good_singletons* > ${file%R1_001_prinseq_good_prinseq_good_singletons*}R_prinseq_good_singletons.fastq; done
	rm *R1_001_prinseq_good_singletons*
	rm *R2_001_prinseq_good_singletons*

##7 Preform gene assemble with SPADES; http://bioinf.spbau.ru/spades. Then re-name SPADES output for use in next steps. Because of the previous BLAST step against the gene only database this will run very quickly.

example commands:

	module load spades/spades-3.5.0
	for file in *R1_001_prinseq_good_prinseq_good*; do sbatch -N 1 -n 3 --wrap="spades.py -1 ${file} -2 ${file%*R1_001_prinseq_good_prinseq_good*}*R2_001_prinseq_good_prinseq_good* -s ${file%*R1_001_prinseq_good_prinseq_good*}R_prinseq_good_singletons.fastq -t 3 -o ${file%*R1_001_prinseq_good_prinseq_good*}spades_out/"; done
	python 7_cp_script.py *_spades_out

Note: The rest of the analysis can be preformed with limited space. If using Lewis Cluster at MU you should copy the directory "spades_contigs" to your data directory and preform the rest of the steps there. Be sure to backup anything in the scratch directory.

##8 Combine overlapping reads using CAP3; http://doua.prabi.fr/software/cap3. 

example commands:

	for file in *.fasta; do cap3 ${file}; done
	for file in *.fasta; do cat ${file}.cap.contigs ${file}.cap.singlets > ${file%.fasta}.cap3.fasta; done

##9 Preform tblastn annotation using 9a and 9b scripts.

example commands:

	python 9a_Reformate_NCBI_file.py database_file.fasta
	for file in *.cap3.fasta; do python 9b_tblastn_anotation_of_contig_files_multipule_genomes_gene_names.py ${file} database_file.fasta; done

##10 Simplify file names for ease

example command:

	python Name_simplification.py *.fasta.fasta
	sed -i -e 's/ .*//g' *.fasta

##11 Add any reference, outgroup, or other previously sequenced organelle genomes you want in the tree (can be same as used for database but must be in nucleotide form). 

Reformat them as shown below:

	for file in *.fasta; do python 9a_Reformate_NCBI_file.py ${file}; done
	for file in *_gene_names.fasta; do mv ${file} ${file%_gene_names.fasta}.fasta; done
	sed -i -e 's/@.*//g' *.fasta 
	sed -i -e 's/ .*//g' *.fasta

##12 Cluster genes for alignment and tree building. IMPORTANT change EXCLUDE_REPETATIVE_GENES to False if you want repetitive genes included

	python Cluster_by_gene_Feb12_15.py 8 *.fasta

##13 Preform mafft alignment

example command:

	module load mafft/mafft-7.221
	sbatch -N 1 -n 10 --wrap="python 13_mafft_wrapper.py 4_name_simplified_and_genomes/gene_clusters_repetative_genes_excluded/ 5_mafft_out/ .fasta 10 DNA"

##14 Clean alignments. Utilizes phyutility; https://code.google.com/p/phyutility/.

	python ../commands/6.5_phyutility_wrapper.py . . 0.3 DNA

##15 Make gene trees using RAxML

example commands:

	module load raxml/raxml-8.1.21
	sbatch -N 1 -n 20 --wrap="python commands/8_RAxML_genetree_wrapper_BS_multi_thread.py /data/jwashbur/Brassiceae_GSS/5_mafft_out/ /data/jwashbur/Brassiceae_GSS/6_RAxML/ DNA 20"

##16 Make concatenated tree. Add a prefix to each of your files:

example commands:

	for file in *.aln-cln; do cp ${file} Chloro${file}; done

Change the file "16_concatenate_matrices.py" so that MATRIX_FILE_HEADING is equal to the beginning several characters of each of your gene files. Then preform the concatenation

example command:

	python 16_concatenate_matrices.py 9_mafft_out_RE/prefix_added/ 40 8 DNA Concatonated_tree_RE/con_out

Make the concatenated tree:

	sbatch -N 1 -n 24 --wrap="raxmlHPC-PTHREADS-AVX -T 24 -f a -x 12345 -# 200 -p 12345 -m GTRCAT -q Chlor_con.model -s Chlor_con.phy -n Chlor_con_BS200"

##17 Make a coalescent tree using ASTRAL; https://github.com/smirarab/ASTRAL

	cat 200BS_gene_trees/RAxML_bestTree.* > Astral/RAxML_bestTree_for_astral.tre

	cp ../200BS_gene_trees/RAxML_bootstrap.* BS_files/

	ll BS_files/* | awk '{print $9}' > BS_file_list.txt  #make bs file list

	sbatch --wrap="java -jar /data/pireslab/software/Astral/astral.4.4.4.jar -i RAxML_bestTree_for_astral.tre -b BS_file_list.txt -o RAxML_bootstrap_Astral_out.txt -r 200"

	tail -n 1 RAxML_bootstrap_Astral_out.txt > RAxML_bootstraps_on_best_Astral_out.tre