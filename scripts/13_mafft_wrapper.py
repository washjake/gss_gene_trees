"""
Created by Ya Yang
Takes a directory of fasta files

If there are >= 1000 sequences in the direction, use --auto
For fasta files with less than 1000 sequences, use the slower but much 
more accurate algorithm

Uncomment the com += "--anysymbol " line if there are "U" or any other unusual
charactors in the sequences
"""

import os,sys

if __name__ == "__main__":
	if len(sys.argv) != 6:
		print "usage: python pep_mafft_wrapper.py inDIR outDIR infile_ending thread DNA/aa"
		sys.exit()
	
	inDIR = sys.argv[1]+"/"
	outDIR = sys.argv[2]+"/"
	file_end = sys.argv[3]
	thread = sys.argv[4]
	
	if sys.argv[5] == "aa":
		seqtype = "--amino"
	elif sys.argv[5] == "DNA":
		seqtype = "--nuc" 	
	else:
		print "Input data type: DNA or aa"
		sys.exit()
		
	done = os.listdir(outDIR)
	filecount = 0
	for i in os.listdir(inDIR):
		if i[-len(file_end):] == file_end and i+".aln" not in done:
			filecount += 1
			#only align files with the designated file ending but not aligned yet
			seqcount = 0 #record how many sequences in the fasta file
			with open(inDIR+i,"r") as infile:
				for line in infile:
					if line[0] == ">": seqcount += 1
			if seqcount >= 1000:
				alg = "--auto" #so that the run actually finishes!
			else:
				alg = "--genafpair --maxiterate 1000"
			com = "mafft "+alg+" "+seqtype+" --thread "+thread+" "
			#com += "--anysymbol " #use this when there are Us in aa sequences
			com += inDIR+i+" > "+outDIR+i+".aln"
			print com
			os.system(com)
	
	if filecount == 0:
		print "No file end with",file_end,"found"
				
