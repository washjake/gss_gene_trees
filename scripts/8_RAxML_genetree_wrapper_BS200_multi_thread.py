#Originally created by Ya Yang. Modified slightly by Jacob Washburn

import os,sys

if len(sys.argv) != 5:
	print "python RAxML_genetree_wrapper.py <abs path to inDIR> <abs path to outDIR> <DNA/aa> <threads>"
	sys.exit(0)

inDIR = sys.argv[1]+"/"
outDIR = sys.argv[2]+"/"
threads = sys.argv[4]

if sys.argv[3] == "aa": m = "PROTCATWAG"
elif sys.argv[3] == "DNA": m = "GTRCAT" 	
else:
	print "Input data type: DNA or aa"
	sys.exit()

os.system("mkdir "+outDIR)
for alignment in os.listdir(inDIR):
	if alignment[-7:] != "aln-cln":
		continue
	#com = "raxmlHPC-SSE3 -f a -x 12345 -# 200 -p 12345 -m "+m+" -V -s "+inDIR+alignment+" -w "+outDIR+" -n "+alignment+".tre --no-bfgs"
	com = "raxmlHPC-PTHREADS-AVX -T "+threads+" -f a -x 12345 -# 200 -p 12345 -m "+m+" -s "+inDIR+alignment+" -w "+outDIR+" -n "+alignment+".tre"
	print com
	os.system(com)
	#os.system("mv RAxML_*."+alignment+".tre "+outDIR)

