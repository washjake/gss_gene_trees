from Bio import SeqIO
from Bio.Alphabet import generic_dna
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import sys
import os

if len(sys.argv) != 3:
	print "usage: python blastn_anotation_of_contig_files.py <nucliotide_contig_file_for_anotation.fasta> <nucliotide_file_of_genes_to_anotate_from.fasta>"
	sys.exit()

print "Making blast database for anotation"
mbdb = "makeblastdb -in "+sys.argv[1]+" -parse_seqids -dbtype nucl -out "+sys.argv[1]+"_nucliotide_data_base_for_blast.fasta"
print mbdb
os.system(mbdb)

print "Running tblastn analysis"
run_blast = "tblastn -db "+sys.argv[1]+"_nucliotide_data_base_for_blast.fasta -query "+sys.argv[2]+" -evalue 0.00001 -out "+sys.argv[1]+"_qseqid_evalue_bitscore_sseqid_sstart_send_sseq -outfmt '6 qseqid evalue bitscore sseqid sstart send sseq'"
print run_blast
os.system(run_blast)

print "Sorting blastn results and selecting the best hit for each gene"
from_blast=open(sys.argv[1]+"_qseqid_evalue_bitscore_sseqid_sstart_send_sseq","r")
output_handle1 = open(sys.argv[1]+"_qseqid_evalue_bitscore_sseqid_sstart_send_sseq_gene_names", "w")
for line in from_blast:
	if len(line) < 3: continue #skip empty lines
	output_handle1.write(line.split("@")[0]+"\t"+line+"\n")
output_handle1.close()
sort_and_select = "sort -k1,1 -k4,4gr -k3,3g "+sys.argv[1]+"_qseqid_evalue_bitscore_sseqid_sstart_send_sseq_gene_names | sort -u -k1,1 --merge > "+sys.argv[1]+"_qseqid_evalue_bitscore_sseqid_sstart_send_sseq_besthit.txt"
print sort_and_select
os.system(sort_and_select)

print "Preforming annotations"
blast_file = open(sys.argv[1]+"_qseqid_evalue_bitscore_sseqid_sstart_send_sseq_besthit.txt","r")
contig_file = sys.argv[1]
gene_file = sys.argv[1]+"_anotated_genes_out_"+sys.argv[2]+".fasta"
output_handle = open(gene_file, "w")

for line in blast_file:
	if len(line) < 3: continue #skip empty lines
	spls = line.strip().split("\t")
	#print spls
	Anotation_name = spls[0]
	Contig_blast_id = spls[4]
	sstart = spls[5]
	send = spls [6]
	#print(Anotation_name,Contig_blast_id,sstart,send)
	contig_file_index = SeqIO.index(contig_file, "fasta", generic_dna)
	record = contig_file_index[Contig_blast_id]
	if int(sstart) < int(send):
		edited = record.seq[int(sstart):int(send)]
		gene_record = SeqRecord(edited,id = Anotation_name,description=record.id)
	else:
		edited = record.seq[int(send):int(sstart)]
		gene_record1 = SeqRecord(edited,id = Anotation_name,description=record.id)
		gene_record = gene_record1.reverse_complement(id = Anotation_name,description=record.id)
	#print(gene_record.format("fasta"))
	#print(Anotation_name)
	print (gene_record)
	print (int(sstart),int(send))
	SeqIO.write(gene_record, output_handle, "fasta")
	continue
output_handle.close()

clean_up = "rm "+sys.argv[1]+"_nucliotide_data_base_for_blast.fasta.n*"
clean_up2 = "rm "+sys.argv[1]+"_qseqid_evalue_bitscore_sseqid_sstart_send_sseq*"
os.system(clean_up)
os.system(clean_up2)
