from Bio import SeqIO
from Bio.Alphabet import generic_dna
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import sys
import os


if len(sys.argv) != 3:
	print "usage: python blastx_sorter3.py <nucleotide_file.fasta> <protien_file_for_database.fasta>"
	sys.exit()
DIR="./"
makedb="TRUE"
makeFASTA="TRUE"
run_blast="TRUE"
for i in os.listdir(DIR):
	if i == sys.argv[2]+"_nucliotide_data_base_for_blast.fasta.phr":
		makedb="FALSE"
	if i == sys.argv[1]+"_conv_to.fasta":
		makeFASTA="FALSE"
		print "Output files for this run already exist. Please delete them first if you wish to re-run the script"
		sys.exit()
	if i==sys.argv[1]+"_sseqid_evalue_bitscore_qseqid_qstart_qend_qseq":
		run_blast="FALSE"


if makedb == "TRUE":
	print "Making blast database for anotation"
	mbdb = "makeblastdb -in "+sys.argv[2]+" -parse_seqids -dbtype prot -out "+sys.argv[2]+"_nucliotide_data_base_for_blast.fasta"
	print mbdb
	os.system(mbdb)
else:
	print "Using existant database"

if makeFASTA=="TRUE":
	print "Makeing fasta file from fastq"
	output_handle1 = open(sys.argv[1]+"_conv_to.fasta", "w")
	for record1 in SeqIO.parse(sys.argv[1], "fastq", generic_dna):
		SeqIO.write(record1, output_handle1, "fasta")
	output_handle1.close()
else:
	print "Using existant converted fasta"

if run_blast=="TRUE":
	print "Running blastx analysis"
	run_blast = "blastx -db "+sys.argv[2]+"_nucliotide_data_base_for_blast.fasta -query "+sys.argv[1]+"_conv_to.fasta"+" -evalue 0.00001 -out "+sys.argv[1]+"_sseqid_evalue_bitscore_qseqid_qstart_qend_qseq -outfmt '6 sseqid evalue bitscore qseqid qstart qend qseq'"
	print run_blast
	os.system(run_blast)
else:
	print "Using existant blast hits file"

print "Sorting blastx results and excluding duplicate hits"
sort_and_select = "sort -u -k4,4 "+sys.argv[1]+"_sseqid_evalue_bitscore_qseqid_qstart_qend_qseq > "+sys.argv[1]+"_sseqid_evalue_bitscore_qseqid_qstart_qend_qseq_unique.txt"
print sort_and_select
os.system(sort_and_select)

print "Generating fastq file with succesful hits"
blast_file = open(sys.argv[1]+"_sseqid_evalue_bitscore_qseqid_qstart_qend_qseq_unique.txt","r")
contig_file = sys.argv[1]
gene_file = sys.argv[1]+"_contigs_that_match_"+sys.argv[2]+".fastq"
output_handle = open(gene_file, "w")

hit=[] # store contig names that match with database
for line in blast_file:
	if len(line) < 3: continue #skip empty lines
	spls = line.strip().split("\t")
	hit.append(spls[3])
	
for record2 in SeqIO.parse(sys.argv[1], "fastq", generic_dna): 
	if record2.id in hit:
		print (record2.id)
		SeqIO.write(record2, output_handle, "fastq")
output_handle.close()

clean_up = "rm "+sys.argv[1]+"_nucliotide_data_base_for_blast.fasta.n*"
clean_up2 = "rm "+sys.argv[1]+"_sseqid_evalue_bitscore_qseqid_qstart_qend_qseq*"
#os.system(clean_up)
os.system(clean_up2)
