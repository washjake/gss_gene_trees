from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
import sys
import os

infile=sys.argv[1]

dict={}
genes=[]
output_handle = open(infile[:len(infile)-6]+"_gene_names.fasta", "w")
for record in SeqIO.parse(infile, "fasta"):
	#print record
	#print record.description.split("=")[1].split("]")[0].lower()
	gene_name=record.description.split("=")[1].split("]")[0].split(" ")[0].lower() 
	if gene_name[:6]=="zemacp" or gene_name[:4]=="orsa": continue
	if gene_name not in genes:
		#print gene_name
		dict[gene_name] =1
		genes.append(gene_name)
	else:
		#print gene_name
		dict[gene_name] += 1
	Out_record = SeqRecord(record.seq, id = gene_name+"@"+record.id[4:len(record.id)], description = record.description)
	SeqIO.write(Out_record, output_handle, "fasta")
print str(len(genes))+" ortholog/homolog groups"
print dict
output_handle.close()