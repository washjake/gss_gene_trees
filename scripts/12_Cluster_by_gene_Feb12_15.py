from Bio import SeqIO
from Bio.Alphabet import generic_dna
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import sys
import os

EXCLUDE_REPETATIVE_GENES=True
taxon_cut_off = sys.argv[1]
count= len(sys.argv)
gene_cluster_DIR="gene_clusters/"
if EXCLUDE_REPETATIVE_GENES==True:
	gene_cluster_DIR="gene_clusters_repetative_genes_excluded/"
os.system("mkdir "+gene_cluster_DIR)
repeat_list=[]
print count
for x in range(2,count):
	print x
	#sed_cmd1 = "sed -i -e 's/lcl|//g' "+sys.argv[x]
	#print (sed_cmd1)
	#os.system(sed_cmd1)
	repeats={}
	for record1 in SeqIO.parse(sys.argv[x], "fasta", generic_dna):
		if record1.id not in repeats:
			repeats[record1.id]=0
		else:
			repeats[record1.id]+=1
			repeat_list.append(sys.argv[x]+"_"+record1.id)
	print repeats		
	for record in SeqIO.parse(sys.argv[x], "fasta", generic_dna):
		#print record
		output_handle = open(gene_cluster_DIR+record.id+".fasta", "a")
		if repeats[record.id]==0:
			Species_added_to_anotation = SeqRecord(record.seq, id = str(sys.argv[x]).split(".")[0].replace("-", "_"), description = record.description)
		else:
			if EXCLUDE_REPETATIVE_GENES: continue
			Species_added_to_anotation = SeqRecord(record.seq, id = str(sys.argv[x]).split(".")[0].replace("-", "_")+"_"+str(repeats[record.id]), description = record.description)
			repeats[record.id]-=1
		#print Species_added_to_anotation
		SeqIO.write(Species_added_to_anotation, output_handle, "fasta")
	print repeats
output_handle.close()
print repeat_list
# remove clusters with less taxa than cut off
for i in os.listdir(gene_cluster_DIR):
	if i[len(i)-6:] != ".fasta": continue
	seq_count = 0
	taxa=[]
	for record in SeqIO.parse(gene_cluster_DIR+i, "fasta", generic_dna):
		if record.id not in taxa:
			taxa.append(record.id)
	#	seq_count +=1
	#print seq_count
	if len(taxa) < int(taxon_cut_off):
		cmd = "rm "+gene_cluster_DIR+i
		print (cmd)
		os.system(cmd)